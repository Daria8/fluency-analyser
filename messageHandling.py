"""
Author: Daria Alexander
"""

import pandas as pd
import eventLogLoader as logload
import datetime as d_t
from datetime import datetime
import csv
import numpy as nm

class messageHandling():

    def __init__(self, contents, number):
        self.contents = contents
        self.messagenumber = number
        self.previousmessage = None # we initialise the previous message
        self.result = {} # nom de la variable resultat, lire les resultats en pandas

    def printmessage(self):
        print(self.messagenumber)
        print(self.contents)

    """
    basic variables
    
    TIME
    """



    def user_message_submit_time(self):
        """
        :return the time when the user sends the message (Timestamp)
        """
        d = self.contents.event_time[self.contents.event_type_t == 'End edit']
        d = d.astype(datetime).values[0]
        self.result['submit_time'] = d
        return self.result['submit_time']

    def first_typing_event(self):
        # return self.contents.endDate[(self.contents.type == 'keyboard')].first
        # convert to list
        l = list(self.contents.event_time[(self.contents.event_type_t == 'Text change')].astype(datetime))
        # if list has any elements, we'll return first, else we'll return datetime(1,1,1)
        ret = l[0]
        self.result['first_event'] = ret
        return self.result['first_event']


    def system_message_reception_time(self):
        """
        :return: the moment when the system gets the message from the user(Timestamp)
        """
        d = self.contents.event_time[self.contents.event_type_t == 'System message']
        d = d.astype(datetime).values[0]
        self.result['message_reception_time'] = d
        return self.result['message_reception_time']


    def user_message_duration(self):
        """
        :return: the duration of user's message (Timedelta)
        """
        b = self.result['first_event']
        d = self.result['submit_time'] - b
        self.result['us_message_duration'] = d
        return self.result['us_message_duration']


    def user_turn_pause_duration(self):
        """
        :return: the duration of the turn pause(Timedelta)
        """
        d = d_t.timedelta(microseconds=0) # a module datetime import datetime
        a = self.result['first_event'] # it is the first of the current message
        if self.previousmessage != None:
            b = self.previousmessage.result['message_reception_time'] #it is a reception time of the previous message
            d = a -b # we extract the previous from the current
        self.result['us_turn_pause_duration'] = d
        return self.result['us_turn_pause_duration']


    def user_turn_duration(self):
        """
        :return: the duration of the turn pause plus the duration of the message (Timedelta)
        """
        a = self.result['us_message_duration']
        b = self.result['us_turn_pause_duration']
        d = a + b
        self.result['us_turn_duration'] = d
        return self.result['us_turn_duration']


    def system_turn_duration(self):
        """
        :return: the time that the system spends on writing a message(Timedelta)
        """
        d = self.result['message_reception_time'] - self.result['submit_time']
        self.result['sys_turn_duration'] = d
        return self.result['sys_turn_duration']


    def total_turn_duration(self):
        """
        :return: the system turn duration plus the user turn duration(Timedelta)
        """
        d = self.result['sys_turn_duration']
        a = self.result['us_turn_duration']
        d += a
        self.result['tot_turn_duration'] = d
        return self.result['tot_turn_duration']

# iloc - we do not take the first element when there is a difference between the beginning of the bot's speaking and
    # we do not take the last one because it comes after "submit"

    def pauses_duration(self):
        """
        :return: the duration of the pauses(Timedelta)
        """
        r = 0
        self.result['pauses_duration'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1]
        a = dt[(dt['minimal_pause'] == True)].sum()["timeDiff"] # if the minimal pause is true
        if a != 0:
            self.result['pauses_duration'] += a
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')] # we also take the pause before submit(Timedelta)
        b = dt1[(dt1['minimal_pause'] == True)].sum()["timeDiff"]
        if b != 0:
            self.result['pauses_duration'] += b
        return self.result['pauses_duration']


    def extract_p_bursts(self):
        """
        The duration of the user message message excluding pauses.
        We do not take the first and the last pause
        :return: the duration of p-bursts(Timedelta)
        """
        count = 0
        self.result['p_bursts'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1]
        a = dt[(dt['minimal_pause'] == False)].sum()["timeDiff"]
        if a != 0:
            self.result['p_bursts'] += a
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')]
        b = dt1[(dt1['minimal_pause'] == False)].sum()["timeDiff"]
        if b != 0:
            self.result['p_bursts'] += b
        return self.result['p_bursts']


    def revisions_duration(self):
        """
        :return: the duration of revision events (Timedelta)
        """
        self.result['revisions_duration'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')]
        a = dt[(dt['revisions'] == True)].sum()["timeDiff"]
        if a != 0:
            self.result['revisions_duration'] +=a
        return self.result['revisions_duration']

    def extract_r_bursts(self):
        """
        The duration of the user message excluding revisions
        :return: the duration of r-bursts(Timedelta)
        """
        count = 0
        self.result['r_bursts'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')]
        a = dt[(dt['revisions'] == False)].sum()["timeDiff"]
        if a != 0:
            self.result['r_bursts'] += a
        return self.result['r_bursts']

    """
    basic variables
    
    CHARACTERS
    """

    def number_of_added_characters(self):
        """
        :return: the number of added characters(int)
        """
        self.result['nb_of_add_char'] = 0
        nb = self.contents[(self.contents['event_type_t'] == 'Text change')]
        sum = nb['nb_add_char'].sum()
        if sum !=0:
            self.result['nb_of_add_char'] +=sum
        return self.result['nb_of_add_char']

    def number_of_deleted_characters(self):
        """
        :return: the number of deleted characters(int)
        """
        self.result['nb_of_del_char'] = 0
        count = 0
        for i in self.contents['characters_delta']:
            if i == -1:
                count +=1
        if count != 0:
            self.result['nb_of_del_char'] +=count
        return self.result['nb_of_del_char']

    def number_of_final_characters(self):
        """
        :return: the number of final characters(added characters-deleted characters)(int)
        """
        self.result['nb_of_fin_char'] = 0
        nb = self.result['nb_of_add_char']
        nb2 = self.result['nb_of_del_char']
        res = nb-nb2
        if res != 0:
            self.result['nb_of_fin_char'] += res
        return self.result['nb_of_fin_char']

    def number_of_revisions(self):
        """
        :returns the number of revisions(int)
        """
        self.result['nb_of_revisions'] = 0
        for i in self.contents['revisions']:
            if i == True:
                self.result['nb_of_revisions']+=1
        return self.result['nb_of_revisions']


    def number_of_pauses(self):
        """
        :return: the number of pauses(int). Not including the first pause and the last(submit) pause
        """
        self.result['nb_of_pauses'] = 0
        self.count = 0
        nb = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1] # we do not take the first pause into account
        #because it is a timedelta between the previous event which was not keybord and current event. We do not take
        #the last keybord because it is not actual typing
        for i in nb['minimal_pause']:
            if i == True:
                self.count +=1
        self.result['nb_of_pauses'] = self.count
        return self.result['nb_of_pauses']

    def number_p_bursts(self):
        """
        :return: the number of p-bursts. Number of pauses +1
        """
        self.result['nb_of_p_bursts'] = 0
        n = 0
        a = self.result['nb_of_add_char']
        if a > 1:
            n = self.result['nb_of_pauses'] +1
        self.result['nb_of_p_bursts'] = n
        return self.result['nb_of_p_bursts']

    def number_r_bursts(self):
        """
        :return: the number of r-bursts(int)
        """
        r = 0
        self.result['nb_of_r_bursts'] = 0
        a = self.result['nb_of_del_char']
        if a == 0:
            r = 1
        else:
            r = a + 1
        self.result['nb_of_r_bursts'] = r
        return self.result['nb_of_r_bursts']

    def number_of_words(self):
        """
        :return: the number of words in the message
        """
        self.result['nb_of_words'] = 0
        d = self.contents.msg_text[self.contents.event_type_t == 'Player message']
        nb_w = len(str(d).split())
        if nb_w != 0:
            self.result['nb_of_words'] = nb_w
        return self.result['nb_of_words']

    """
    OUTCOME VARIABLES
    """

    def mean_pause_duration(self):
        """
        :return: the duration of pauses divided by the number of pauses(Timedelta)
        """
        self.result['mean_pause_duration'] = d_t.timedelta(microseconds=0)
        np = self.result['nb_of_pauses']
        if np != 0:
            pd = self.result['pauses_duration']/ np
            self.result['mean_pause_duration'] = pd
        return self.result['mean_pause_duration']


    def mean_duration_p_bursts(self):
        """
        :return: the duration of p-bursts divided by the number of p-bursts(Timedelta)
        """
        self.result['mean_duration_p_bursts'] = d_t.timedelta(microseconds=0)
        nbp = self.result['nb_of_p_bursts']
        if nbp != 0:
            pbd = self.result['p_bursts']/ nbp
            self.result['mean_duration_p_bursts'] = pbd.total_seconds()
        return self.result['mean_duration_p_bursts']

    def mean_duration_r_bursts(self):
        """
        :return: the duration of r-bursts divided by the number of r-bursts (Timedelta)
        """
        self.result['mean_duration_r_bursts'] = d_t.timedelta(microseconds=0)
        nbr = self.result['nb_of_r_bursts']
        if nbr != 0:
            rbd = self.result['r_bursts']/nbr
            self.result['mean_duration_r_bursts'] = rbd.total_seconds()
        return self.result['mean_duration_r_bursts']

    def active_writing_duration_proportion(self):
        """
        :return: active writing duration divided by user turn duration(float)
        """
        self.result['active_writing_duration_proportion'] = d_t.timedelta(microseconds=0)
        act_dur = self.result['p_bursts'] /self.result['us_turn_duration']
        self.result['active_writing_duration_proportion'] = act_dur
        return self.result['active_writing_duration_proportion']

    def mean_length_p_bursts(self):
        """
        :return: the number of added characters divided by the number of p-bursts(int/float)
        """
        self.result['mean_length_of_p_bursts'] = 0
        nbp = self.result['nb_of_p_bursts']
        if nbp != 0:
            mlpb = self.result['nb_of_add_char']/ nbp
            self.result['mean_length_of_p_bursts'] = mlpb
        return self.result['mean_length_of_p_bursts']

    def mean_length_r_bursts(self):
        """
        :return: the number of added characters divided by the number of r-bursts(float)
        """
        self.result['mean_length_of_r_bursts'] = 0
        nbr = self.result['nb_of_r_bursts']
        if nbr != 0:
            mlrb = self.result['nb_of_add_char']/nbr
            self.result['mean_length_of_r_bursts'] = mlrb

    def add_char_act_dur(self):
        """
        :return: the number of added characters divided by the active writing duration(float)
        """
        self.result['nb_char_act_wr_duration'] = 0
        avd = self.result['p_bursts'].total_seconds()
        if avd != 0:
            acad = self.result['nb_of_add_char']/avd
            self.result['nb_char_act_wr_duration'] = acad
        return self.result['nb_char_act_wr_duration']

    def words_act_dur(self):
        """
        :return: the number of words divided per active writing duration
        """
        self.result['nb_words_act_wr_duration'] = 0
        avd = self.result['p_bursts'].total_seconds()
        if avd != 0:
            wad = self.result['nb_of_words']/avd
            self.result['nb_words_act_wr_duration'] = round(wad,3)
        return self.result['nb_words_act_wr_duration']

    def add_char_user_turn_dur(self):
        """
        :return: the number of added characters divided by the user turn duration(float)
        """
        self.result['add_char_us_turn_duration'] = 0
        utd = self.result['us_turn_duration'].total_seconds()
        if utd != 0:
            acutd = self.result['nb_of_add_char']/ utd
            self.result['add_char_us_turn_duration'] = acutd
        return self.result['add_char_us_turn_duration']

    def fin_char_act_wr_dur(self):
        """
        :return: the number of characters divided by the active writing duration(float)
        """
        self.result['final_char_act_wr_duration'] = 0
        awd = self.result['p_bursts'].total_seconds()
        if awd != 0:
            fcad = self.result['nb_of_fin_char']/awd
            self.result['final_char_act_wr_duration'] = fcad
        return self.result['final_char_act_wr_duration']


    def revision_episodes_active_wr(self):
        """
        :return: the number of revisions divided by the active writing time(float)
        """
        self.result['revisions_active_wr_duration'] = 0
        avd = self.result['p_bursts'].total_seconds()
        if avd != 0:
            rat = self.result['nb_of_revisions']/avd
            self.result['revisions_active_wr_duration'] = rat
        return self.result['revisions_active_wr_duration']

    def revision_episodes_per_words(self):
        """
        :return: the number of revisions divided by the number of words
        """
        self.result['revisions_per_words'] = 0
        w = self.result['nb_of_words']
        r = self.result['nb_of_revisions']
        if w != 0:
            repw = r/w
            self.result['revisions_per_words'] = repw
        return self.result['revisions_per_words']


    def revision_proportion(self):
        """
        :return: number of final characters divded by the number of added characters(float)
        """
        self.result['revision_proportion'] = 0
        nbac = self.result['nb_of_add_char']
        if nbac != 0:
            rp = self.result['nb_of_fin_char']/ nbac
            self.result['revision_proportion'] = rp
        return self.result['revision_proportion']

