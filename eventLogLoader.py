"""
Author: Daria Alexander
"""

import pandas as pd
import json
import numpy as nm

class eventLogLoader():

    #constructor
    def __init__(self):
        self.events = None

    def load(self, path_json):
        try :
            with open(path_json, encoding='utf-8') as data_file:
                data = json.loads(data_file.read())
                self.events = pd.DataFrame(data)
        except (FileNotFoundError, IOError):
            print ("File not found")
        #rename the columns in order that their names match imputlog format


        self.events = self.events[self.events.event_type != 1] # delete the events that have eventsType 1

        # поправим тип у дат со строк на datetime64
        self.events['event_time'] = pd.to_datetime(self.events['event_time'])



    # we compute other variables that we need
    # extract the added character
    def extractChar(self):
        self.events['output'] = self.extractDelta(self.events.event_text.shift(1), self.events.event_text)

    # extract the length difference of the line
    def extractLengthDiff(self):
        dfr = self.events[self.events.event_type_t == 'Text change']
        self.events['characters_delta'] = dfr.event_text.str.len() - dfr.shift(1).event_text.str.len()


    def nb_add_char(self):
        self.events['nb_add_char'] = self.events.characters_delta.abs()
        #Для типа 'hear' меняем значение в колонке charactersDelta
        self.events.loc[self.events.event_type_t == 'TTS voicing start', 'characters_delta'] = 1
        #if self.events.type == 'hear':
         #   self.events.charactersDelta = 1

    # extract the characters if they were added
    def extractDelta(self, previousString, currentString):
        addedChars = ""
        #if there is a difference between the previous string and the next string
        for i, s in enumerate(self.events.ndiff(previousString, currentString)):
            # continue if there is no added characters
            if s[0] == ' ': continue
            # if there are added characters
            elif s[0] == '+':
            #extract them
                addedChars += u'{}'.format(s[-1],i)
            elif s[0] == '-':
                addedChars = 'BACK'
        return addedChars;

    def extractPauses(self, minimalPauseTreshold = 250):
        self.events = self.events.assign(timeDiff = self.events.event_time.diff())
        self.events['minimal_pause'] = self.events.timeDiff > pd.Timedelta(str(minimalPauseTreshold)+'ms')
        #self.events['Pause500'] = self.events.timeDiff > pd.Timedelta('500ms')
        #self.events['Pause1000'] = self.events.timeDiff > pd.Timedelta('1000ms')
        #self.events['Pause2000'] = self.events.timeDiff > pd.Timedelta('2000ms')
        #self.events['Pause200'] = self.events.timeDiff > pd.Timedelta('200ms')
        #self.events['Pause300'] = self.events.timeDiff > pd.Timedelta('300ms')

    def revisions(self):
            self.events['revisions'] = self.events.characters_delta <= -1

    def measures_columns(self):
        self.events['mean_p_bursts'] = nm.nan
        self.events['mean_r_bursts'] = nm.nan
        self.events['active_wr_dur_prop'] = nm.nan
        self.events['mean_l_p_bursts'] = nm.nan
        self.events['mean_l_r_bursts'] = nm.nan
        self.events['add_char_act_dur'] = nm.nan
        self.events['words_act_dur'] = nm.nan
        self.events['words_act_dur'] = self.events['words_act_dur'].replace(nm.nan,0)
        self.events['add_char_us_turn_dur'] = nm.nan
        self.events['fin_char_act_dur'] = nm.nan
        self.events['revision_prop'] = nm.nan


    # we save the data in the test file
    def saveData(self):
        writer = self.events.to_csv("test.csv", sep=';')

    #we are getting the events
    def getEvents(self):
            return self.events


# we are doing the main
if __name__ == '__main__' :

    matches = eventLogLoader()
    matches.load('logs_events.json')
    matches.extractLengthDiff()
    matches.extractPauses()
    matches.revisions()
    matches.nb_add_char()
    matches.measures_columns()
    matches.saveData()
    print(matches.events.iloc[1])
    print(matches.events.iloc[1])

    res = matches.getEvents()
    print(res)
