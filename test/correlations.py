import pandas as pd
import numpy as np

data = pd.read_csv("C:/Users/Daria Alexander/PycharmProjects/fluency-an-fin/norm/mdrb.csv", encoding='utf-8', sep=';')

data['norm'] = (data['mean_l_p_bursts'] - data['mean_l_p_bursts'].min())/(data['mean_l_p_bursts'].max()-data['mean_l_p_bursts'].min())

#print(data)

dt = data.groupby(['user_id','user_vocsize'], as_index=False)['mean_dur_r_bursts'].mean()

c = dt.to_csv("te_corr.csv", sep=';')

#print(dt)

df = pd.DataFrame({'Name': ['Geeks', 'Peter', 'James', 'Jack', 'Lisa'],
                   'Team': ['Boston', 'Boston', 'Boston', 'Chele', 'Barse'],
                   'Position': ['PGinn', 'PGjuj', 'UG', 'PG', 'UG'],
                   'Number': [3, 4, 7, 11, 5],
                   'Age': [33, 25, 34, 35, 28],
                   'Height': ['6-2', '6-4', '5-9', '6-1', '5-8'],
                   'Weight': [89, 79, 113, 78, 84],
                   'College': ['MIT', 'MIT', 'MIT', 'Stanford', 'Stanford'],
                   'Salary': [99999, 99994, 89999, 78889, 87779]},
                   index =['ind1', 'ind2', 'ind3', 'ind4', 'ind5'])
#print(df, "\n")

print(df['Age'].mean())
# for dividing into data

df1 = df[df['Position'].str.contains("PG")]
print(df1)

# splitting into chunks
x = np.array_split(df1, 3)

print("x0",x[0])
print("x1",x[1])
print("x2",x[2])
