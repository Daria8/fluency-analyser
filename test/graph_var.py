import pandas as pd
import numpy as np

"""
mlpb
"""

data = pd.read_csv("C:/Users/Daria Alexander/PycharmProjects/fluency-an-fin/norm/mlpb1000_corr.csv", encoding='utf-8', sep=';')

data['norm'] = (data['mean_l_p_bursts'] - data['mean_l_p_bursts'].min())/(data['mean_l_p_bursts'].max()-data['mean_l_p_bursts'].min())

dat_1 = data[data['conv_init_time'].str.contains("2018-09-21") | data['conv_init_time'].str.contains("2018-11-06")|\
             data['conv_init_time'].str.contains("2018-10-15")| data['conv_init_time'].str.contains("2018-10-16 06")| \
             data['conv_init_time'].str.contains("2018-10-16 07")]

m_s1 = dat_1['norm'].mean()
print("session1_mlpb",m_s1)

writer = dat_1.to_csv("ch2.csv", sep=';')

u1 = dat_1[dat_1['user_id'] == 1291]

m_u1 = u1['norm'].mean()
print("s1 u1", m_u1)


dat_2 = data[data['conv_init_time'].str.contains("2018-09-24") |\
             data['conv_init_time'].str.contains("2018-09-25")|data['conv_init_time'].str.contains("2018-11-05")\
             | data['conv_init_time'].str.contains("2018-11-07")| data['conv_init_time'].str.contains("2018-10-16 09")]

m_s2 = dat_2['norm'].mean()
print("session2_mlpb",m_s2)

u1_2 = dat_2[dat_2['user_id'] == 1291]

m_u1_2 = u1_2['norm'].mean()
print("s2 u1", m_u1_2)


dat_3 = data[data['conv_init_time'].str.contains("2018-09-27") \
             | data['conv_init_time'].str.contains("2018-09-26")| data['conv_init_time'].str.contains("2018-10-23")|\
             data['conv_init_time'].str.contains("2018-11-08")|data['conv_init_time'].str.contains("2018-11-09")|\
             data['conv_init_time'].str.contains("2018-11-12")]

m_s3 = dat_3['norm'].mean()
print("session3_mlpb",m_s3)

u1_3 = dat_3[dat_3['user_id'] == 1291]

m_u1_3 = u1_3['norm'].mean()
print("s3 u1", m_u1_3)




dat = pd.read_csv("C:/Users/Daria Alexander/PycharmProjects/fluency-an-fin/norm/mdpb300_corr.csv", encoding='utf-8', sep=';')

dat['norm'] = (dat['mean_p_bursts'] - dat['mean_p_bursts'].min())/(dat['mean_p_bursts'].max()-dat['mean_p_bursts'].min())

dat_4 = dat[dat['conv_init_time'].str.contains("2018-09-21") |\
            dat['conv_init_time'].str.contains("2018-10-15")| dat['conv_init_time'].str.contains("2018-10-16 06")|\
            dat['conv_init_time'].str.contains("2018-11-06")|
            dat['conv_init_time'].str.contains("2018-10-16 07")]


m_s4 = dat_4['norm'].mean()
print("session1_mdpb",m_s4)


dat_5 = dat[dat['conv_init_time'].str.contains("2018-09-24") |\
            dat['conv_init_time'].str.contains("2018-09-25")|\
            dat['conv_init_time'].str.contains("2018-11-05")| dat['conv_init_time'].str.contains("2018-11-07")|\
            dat['conv_init_time'].str.contains("2018-10-16 09")]

writer = dat_5.to_csv("ch1.csv", sep=';')

m_s5 = dat_5['norm'].mean()
print("session2_mdpb",m_s5)


dat_6 = dat[dat['conv_init_time'].str.contains("2018-09-27") |\
            dat['conv_init_time'].str.contains("2018-09-26")|\
            dat['conv_init_time'].str.contains("2018-10-23")|dat['conv_init_time'].str.contains("2018-11-08")|\
            dat['conv_init_time'].str.contains("2018-11-09")|\
            dat['conv_init_time'].str.contains("2018-11-12")]

m_s6 = dat_6['norm'].mean()
print("session3_mdpb",m_s6)
