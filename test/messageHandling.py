"""
Author: Daria Alexander
"""

import pandas as pd
import eventLogLoader as logload
import datetime as d_t
from datetime import datetime
import csv
import numpy as nm
from nltk.tokenize import RegexpTokenizer

class messageHandling():

    def __init__(self, contents, number):
        self.contents = contents # Pandas dataframe
        self.messagenumber = number # Message number
        self.previousmessage = None # we initialise the previous message
        self.result = {} # nom de la variable resultat, lire les resultats en pandas

    def printmessage(self):
        print(self.messagenumber)
        print(self.contents)

    """
    basic variables
    
    TIME
    """



    def user_message_submit_time(self):
        """
        :return the time when the user sends the message (Timestamp)
        """
        d = self.contents.event_time[self.contents.event_type_t == 'End edit'].iloc[-1]
        self.result['submit_time'] = d
        return self.result['submit_time']

    def first_typing_event(self):
        """
        :return: the first typing event (Timestamp)
        """
        # return self.contents.endDate[(self.contents.type == 'keyboard')].first
        # convert to list
        l = list(self.contents.event_time[(self.contents.event_type_t == 'Text change')].astype(datetime))
        # if list has any elements, we'll return first, else we'll return datetime(1,1,1)
        ret = l[0]
        self.result['first_event'] = ret
        return self.result['first_event']


    def system_message_reception_time(self):
        """
        :return: the moment when the system gets the message from the user(Timestamp)
        """
        d = self.contents.event_time[self.contents.event_type_t == 'System message']
        d = d.astype(datetime).values[0]
        self.result['message_reception_time'] = d
        return self.result['message_reception_time']


    def user_message_duration(self):
        """
        :return: the duration of user's message (Timedelta)
        """
        b = self.result['first_event']
        d = self.result['submit_time'] - b
        self.result['us_message_duration'] = d
        return self.result['us_message_duration']


    def user_turn_pause_duration(self):
        """
        :return: the duration of the turn pause(Timedelta)
        """
        d = d_t.timedelta(microseconds=0) # a module datetime import datetime
        a = self.result['first_event'] # it is the first of the current message
        if self.previousmessage != None:
            b = self.previousmessage.result['message_reception_time'] #it is a reception time of the previous message
            d = a -b # we extract the previous from the current
        self.result['us_turn_pause_duration'] = d
        return self.result['us_turn_pause_duration']


    def user_turn_duration(self):
        """
        :return: the duration of the turn pause plus the duration of the message (Timedelta)
        """
        a = self.result['us_message_duration']
        b = self.result['us_turn_pause_duration']
        d = a + b
        self.result['us_turn_duration'] = d
        return self.result['us_turn_duration']


    def system_turn_duration(self):
        """
        :return: the time that the system spends on writing a message(Timedelta)
        """
        d = self.result['message_reception_time'] - self.result['submit_time']
        self.result['sys_turn_duration'] = d
        return self.result['sys_turn_duration']


    def total_turn_duration(self):
        """
        :return: the system turn duration plus the user turn duration(Timedelta)
        """
        d = self.result['sys_turn_duration']
        a = self.result['us_turn_duration']
        d += a
        self.result['tot_turn_duration'] = d
        return self.result['tot_turn_duration']

# iloc - we do not take the first element when there is a difference between the beginning of the bot's speaking and
    # we do not take the last one because it comes after "submit"

    def pauses_duration(self):
        """
        :return: the duration of the pauses(Timedelta)
        """
        r = 0
        self.result['pauses_duration'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1]
        a = dt[(dt['minimal_pause'] == True)].sum()["timeDiff"] # if the minimal pause is true
        if a != 0:
            self.result['pauses_duration'] += a
        pe = self.contents.minimal_pause[(self.contents['event_type_t'] == 'End edit')].iloc[-1] # we also take the pause before submit(Timedelta)
        if pe == True:
            ep = self.contents.timeDiff[(self.contents['event_type_t'] == 'End edit')].iloc[-1]
            self.result['pauses_duration'] += ep
        return self.result['pauses_duration']


    def extract_p_bursts(self):
        """
        The duration of the user message message excluding pauses.
        :return: the duration of p-bursts(Timedelta)
        """
        self.result['p_bursts'] = d_t.timedelta(microseconds=0)
        md = self.result['us_message_duration']
        pd = self.result['pauses_duration']
        if md != 0:
            pb = md - pd
        if md != 0:
            self.result['p_bursts'] = pb
        return self.result['p_bursts']

    def revisions_duration(self):
        """
        :return: the duration of revision events (Timedelta)
        """
        self.result['revisions_duration'] = d_t.timedelta(microseconds=0)
        dt = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1]
        a = dt[(dt['revisions'] == True)].sum()["timeDiff"]
        if a != 0:
            self.result['revisions_duration'] +=a
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')] # we also take the revision before submit(Timedelta)
        b = dt1[(dt1['revisions'] == True)].sum()["timeDiff"]
        if b != 0:
            self.result['revisions_duration'] += b #if the revision before "end edit" exists, we add it to the duration of revisions
        return self.result['revisions_duration']

    def extract_r_bursts(self):
        """
        The duration of the user message excluding revisions
        :return: the duration of r-bursts(Timedelta)
        """
        self.result['r_bursts'] = d_t.timedelta(microseconds=0)
        md = self.result['us_message_duration']
        rd = self.result['revisions_duration']
        if md != 0:
            rb = md - rd
        if rb != 0:
            self.result['r_bursts'] = rb
        return self.result['r_bursts']

    """
    basic variables
    
    CHARACTERS
    """

    def number_of_added_characters(self):
        """
        :return: the number of added characters(int)
        """

        self.result['nb_of_add_char'] = 0
        count = 0
        for i in self.contents['characters_delta']:
            if i >= 1:
                count +=i
        if count != 0:
            self.result['nb_of_add_char'] +=count
        return self.result['nb_of_add_char']

    def number_of_deleted_characters(self):
        """
        :return: the number of deleted characters(int)
        """
        self.result['nb_of_del_char'] = 0
        count = 0
        for i in self.contents['characters_delta']:
            if i <= -1:
                count +=abs(i)
        if count != 0:
            self.result['nb_of_del_char'] +=count
        return self.result['nb_of_del_char']

    def number_of_final_characters(self):
        """
        :return: the number of final characters(added characters-deleted characters)(int)
        """
        self.result['nb_of_fin_char'] = 0
        nb = self.result['nb_of_add_char']
        nb2 = self.result['nb_of_del_char']
        res = nb-nb2
        if res != 0:
            self.result['nb_of_fin_char'] += res
        return self.result['nb_of_fin_char']

    def number_of_revisions(self):
        """
        :returns the number of revisions(int)
        """
        self.result['nb_of_revisions'] = 0
        for i in self.contents['revisions']:
            if i == True:
                self.result['nb_of_revisions']+=1
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')] # we also take the revision before submit(Timedelta)
        v = dt1['revisions'].values[0]
        if v == True:
            self.result['nb_of_revisions']+=1
        return self.result['nb_of_revisions']


    def number_of_pauses(self):
        """
        :return: the number of pauses(int). Not including the first pause and the last(submit) pause
        """
        self.result['nb_of_pauses'] = 0
        count = 0
        nb = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[1:-1] # we do not take the first pause into account
        #because it is a timedelta between the previous event which was not keybord and current event. We do not take
        #the last keybord because it is not actual typing
        for i in nb['minimal_pause']:
            if i == True:
                count +=1
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')] # we also take the event before submit(Timedelta)
        v = dt1['minimal_pause'].values[0]
        if v == True: # if it is a pause, we add + 1 to pauses
            count += 1
        self.result['nb_of_pauses'] = count
        return self.result['nb_of_pauses']

    def number_p_bursts(self):
        """
        :return: the number of p-bursts(int). If the last event is a pause
        number of p-bursts = number of pauses. If not, it is the number of pauses + 1
        """
        self.result['nb_of_p_bursts'] = 0
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')]
        v = dt1['minimal_pause'].values[0] # if the last event is a pause
        if v == True:
            self.result['nb_of_p_bursts'] = self.result['nb_of_pauses']
        else:
            n = 0
            a = self.result['nb_of_add_char']
            if a > 1:
                n = self.result['nb_of_pauses'] +1
            self.result['nb_of_p_bursts'] = n
        return self.result['nb_of_p_bursts']

    def number_r_bursts(self):
        """
        :return: the number of r-bursts(int)
        If the last event is a revision, the number of r-bursts = the number of revisions.
        Else it is a number of revisions + 1
        """
        r = 0
        self.result['nb_of_r_bursts'] = 0
        dt1 = self.contents[(self.contents['event_type_t'] == 'End edit')]
        v = dt1['revisions'].values[0]
        if v == True: # if the last event is a revision
            self.result['nb_of_r_bursts'] = self.result['nb_of_revisions'] # the number of r-bursts equals the number of revisions
        else:
            a = self.result['nb_of_del_char']
            if a == 0: # if there are no revisions
                r = 1 #there will be one revision burst
            else:
                r = a + 1 # otherwise it is the number of revisions + 1
            self.result['nb_of_r_bursts'] = r
        return self.result['nb_of_r_bursts']

    def number_of_words(self):
        """
        :return: the number of words in the message(int)
        """
        self.result['nb_of_words'] = 0
        if len(self.contents[(self.contents['event_type_t'] == 'Text change')]) > 1: # we cannot take the previous line of the last line
            # if there is NO previous line. Otherwise there is an IndexOutOfBounds
            l = self.contents[(self.contents['event_type_t'] == 'Text change')].iloc[-2]
            tokenizer = RegexpTokenizer(r'\w+')
            text = str(l['msg_text'])
            tokens = tokenizer.tokenize(text)
            if len(tokens) != 0:
                self.result['nb_of_words'] = len(tokens)
        return self.result['nb_of_words']

    """
    OUTCOME VARIABLES
    """

    """
    speed fluency
    """



    def add_char_user_mess_dur(self):
        """
        :return: the number of characters divided by user message duration(float)
        """
        self.result['add_char_user_mess_duration'] = 0
        umd = self.result['us_message_duration'].total_seconds()
        if umd != 0:
            acumd = self.result['nb_of_add_char']/umd
            self.result['add_char_user_mess_duration'] = round(acumd,3)
        return self.result['add_char_user_mess_duration']


    def add_char_user_turn_dur(self):
        """
        :return: the number of added characters divided by the user turn duration(float)
        """
        self.result['add_char_us_turn_duration'] = 0
        utd = self.result['us_turn_duration'].total_seconds()
        if utd != 0:
            acutd = self.result['nb_of_add_char']/ utd
            self.result['add_char_us_turn_duration'] = round(acutd,3)
        return self.result['add_char_us_turn_duration']


    def fin_char_us_mess_dur(self):
        """
        :return: the number of characters divided by user message duration (float)
        """
        self.result['final_char_us_mess_duration'] = 0
        umd = self.result['us_message_duration'].total_seconds()
        if umd != 0:
            fcum = self.result['nb_of_fin_char']/umd
            self.result['final_char_us_mess_duration'] = round(fcum,3)
        return self.result['final_char_us_mess_duration']

    def fin_char_us_turn_dur(self):
        """
        :return: the number of characters divided by user turn duration(float)
        """
        self.result['final_char_us_turn_dur'] = 0
        utd = self.result['us_turn_duration'].total_seconds()
        if utd != 0:
            fcut = self.result['nb_of_fin_char']/utd
            self.result['final_char_us_turn_dur'] = round(fcut,3)
        return self.result['final_char_us_turn_dur']


    def words_mess_dur(self):
        """
        :return: the number of words divided per active writing duration (float)
        """
        self.result['nb_words_us_mess_duration'] = 0
        umd = self.result['us_message_duration'].total_seconds()
        if umd != 0:
            wad = self.result['nb_of_words']/umd
            self.result['nb_words_us_mess_duration'] = round(wad,3)
        return self.result['nb_words_us_mess_duration']

    def words_turn_dur(self):
        """
        :return: the number of words divided per active writing duration (float)
        """
        self.result['nb_words_us_turn_duration'] = 0
        utd = self.result['us_turn_duration'].total_seconds()
        if utd != 0:
            wad = self.result['nb_of_words']/utd
            self.result['nb_words_us_turn_duration'] = round(wad,3)
        return self.result['nb_words_us_turn_duration']

    """
    breakdown fluency
    """

    def mean_pause_duration(self):
        """
        :return: the duration of pauses divided by the number of pauses(seconds)
        """
        self.result['mean_pause_duration'] = d_t.timedelta(microseconds=0)
        np = self.result['nb_of_pauses']
        if np != 0:
            pd = self.result['pauses_duration']/ np
            self.result['mean_pause_duration'] = round(pd.total_seconds(),3)
        return self.result['mean_pause_duration']

    def percentage_pause_time(self):
        """
        :return: the percentage of pause time in user message writing time(percent)
        """
        self.result['percentage_pause'] = 0
        p = self.result['pauses_duration'].total_seconds()
        umt = self.result['us_message_duration'].total_seconds()
        if p != 0:
            if umt != 0:
                pp = p/umt
                pps = pp * 100
                self.result['percentage_pause'] = int(pps)
        return self.result['percentage_pause']

    """
    repair fluency
    """

    def revision_episodes_mess_dur(self):
        """
        :return: the number of revisions divided by the active writing time(float)
        """
        self.result['revisions_us_mess_duration'] = 0
        umd = self.result['us_message_duration'].total_seconds()
        if umd != 0:
            rat = self.result['nb_of_revisions']/umd
            self.result['revisions_us_mess_duration'] = round(rat,3)
        return self.result['revisions_us_mess_duration']

    def revision_episodes_turn_dur(self):
        """
        :return: the number of revisions divided by the active writing time(float)
        """
        self.result['revisions_us_turn_duration'] = 0
        utd = self.result['us_turn_duration'].total_seconds()
        if utd != 0:
            rat = self.result['nb_of_revisions']/utd
            self.result['revisions_us_turn_duration'] = round(rat,3)
        return self.result['revisions_us_turn_duration']

    def revision_episodes_per_words(self):
        """
        :return: the number of revisions divided by the number of words(float)
        """
        self.result['revisions_per_words'] = 0
        w = self.result['nb_of_words']
        r = self.result['nb_of_revisions']
        if w != 0:
            repw = r/w
            self.result['revisions_per_words'] = round(repw,3)
        return self.result['revisions_per_words']


    def revision_proportion(self):
        """
        :return: number of final characters divded by the number of added characters(float)
        """
        self.result['revision_proportion'] = 0
        nbac = self.result['nb_of_add_char']
        if nbac != 0:
            rp = self.result['nb_of_fin_char']/ nbac
            self.result['revision_proportion'] = round(rp,3)
        return self.result['revision_proportion']

    """
    automatization
    """


    def mean_duration_p_bursts(self):
        """
        :return: the duration of p-bursts divided by the number of p-bursts(seconds)
        """
        self.result['mean_duration_p_bursts'] = d_t.timedelta(microseconds=0)
        nbp = self.result['nb_of_p_bursts']
        if nbp != 0:
            pbd = self.result['p_bursts']/ nbp
            self.result['mean_duration_p_bursts'] = round(pbd.total_seconds(),3)
        return self.result['mean_duration_p_bursts']

    def mean_duration_r_bursts(self):
        """
        :return: the duration of r-bursts divided by the number of r-bursts (seconds)
        """
        self.result['mean_duration_r_bursts'] = d_t.timedelta(microseconds=0)
        nbr = self.result['nb_of_r_bursts']
        if nbr != 0:
            rbd = self.result['r_bursts']/nbr
            self.result['mean_duration_r_bursts'] = round(rbd.total_seconds(),3)
        return self.result['mean_duration_r_bursts']


    def mean_length_p_bursts(self):
        """
        :return: the number of added characters divided by the number of p-bursts(float)
        """
        self.result['mean_length_of_p_bursts'] = 0
        nbp = self.result['nb_of_p_bursts']
        if nbp != 0:
            mlpb = self.result['nb_of_add_char']/ nbp
            self.result['mean_length_of_p_bursts'] = round(mlpb,3)
        return self.result['mean_length_of_p_bursts']

    def mean_length_r_bursts(self):
        """
        :return: the number of added characters divided by the number of r-bursts(float)
        """
        self.result['mean_length_of_r_bursts'] = 0
        nbr = self.result['nb_of_r_bursts']
        if nbr != 0:
            mlrb = self.result['nb_of_add_char']/nbr
            self.result['mean_length_of_r_bursts'] = round(mlrb,3)
        return self.result['mean_length_of_r_bursts']

    """
    def active_writing_duration_proportion(self):
        
        #:return: active writing duration divided by user turn duration(float)
        
        self.result['active_writing_duration_proportion'] = d_t.timedelta(microseconds=0)
        act_dur = self.result['p_bursts'] /self.result['us_turn_duration']
        self.result['active_writing_duration_proportion'] = act_dur
        return self.result['active_writing_duration_proportion']

    """


