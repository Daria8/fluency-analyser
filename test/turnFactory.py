"""
Author: Daria Alexander
"""

import csv
import pandas as pd
import numpy as nm

# we import the class eventLogLoader
import eventLogLoader as logload

import messageHandling as mymessage
from datetime import datetime
from datetime import timedelta
#import datetime

class turnFactory():

    #constructor
    def __init__(self):
        self.data = None # Pandas dataframe
        self.messagelist =  {} # a dictionary of messages

    # initialise the methods from EventLogLoader

    def initialiseFactory(self, path_json):
        """
        :param path_json:
        initialises turn factory by creating an object of a class EventLogLoader
        """
        try:
            initializer = logload.eventLogLoader()
            initializer.load(path_json)
            initializer.extractLengthDiff()
            initializer.extractPauses()
            initializer.revisions()
            initializer.measures_columns()
            self.data = initializer.getEvents()
        except (FileNotFoundError, IOError):
            print("File not found")

    def getTurns(self):
        return

    def identifyTurns(self):
        """
        computes fluency measures by creating objects of the class MessageHandling
        writes turns and turn ids in the dictionary messagelist
        computes fluency measures on the level of each turn and writes them in the dictionary results of the class MessageHandling
        writes the results in a csv file
        """

        # We obtain series, that contain the messages uniques id's

        turnId_series = self.data['msg_id'].unique()
        #print(len(turnId_series))

        #a loop over all the turn id's
        for turnId in turnId_series:
            #we transfer the contents to the messageHandling class
            if not nm.isnan(turnId): # changer: np
                contents = self.data.loc[self.data['msg_id'] == turnId] # cоздаём экземпляр класса и загр туда информацию
                # которую фильтрцем по номеру сообщения
                currentmessage = mymessage.messageHandling(contents, turnId)# эту информацию передаём конструктору messageHandling
                self.messagelist[turnId] = currentmessage

       # b = datetime.datetime(1,1,1)
        load = logload.eventLogLoader()
        b = datetime(1,1,1)
        previousmessage = None
        with open ('results.csv', 'w') as csv_file:
            writer = csv.writer(csv_file, delimiter=';')
            need_write_fields = True
            for k, single_instance in self.messagelist.items(): # we loop over all the keys in the dictionnary messagelist
                single_instance.previousmessage = previousmessage # single_instance belongs to the messageHandling class
                previousmessage = single_instance

                """
                Basic variables
                """
                sub_t = single_instance.user_message_submit_time()
                #print("submit time", turnId, sub_t, 'message', single_instance.messagenumber)
                first_typ = single_instance.first_typing_event()
                #print("first typing event",turnId, first_typ, 'message', single_instance.messagenumber)
                rec_t = single_instance.system_message_reception_time()
                #print("system message reception time wr", turnId, rec_t, 'message', single_instance.messagenumber)
                us_mess_dur = single_instance.user_message_duration()
                #print("user message duration", turnId, us_mess_dur, 'message', single_instance.messagenumber)
                turn_paus_dur = single_instance.user_turn_pause_duration()
                #print("user turn duration", turn_paus_dur, 'message', single_instance.messagenumber)
                us_turn_dur = single_instance.user_turn_duration()
                #print("user turn duration", us_turn_dur, 'message', single_instance.messagenumber)
                sys_turn_dur = single_instance.system_turn_duration()
                #print(sys_turn_dur, 'message', single_instance.messagenumber)
                tot_turn_dur = single_instance.total_turn_duration()
                #print(tot_turn_dur, 'message', single_instance.messagenumber)
                pause_dur = single_instance.pauses_duration()
                #print(pause_dur, 'message', single_instance.messagenumber)
                p_bursts = single_instance.extract_p_bursts()
                revisions = single_instance.revisions_duration()
                r_bursts = single_instance.extract_r_bursts()
                nb_add_char = single_instance.number_of_added_characters()
                nb_del_char = single_instance.number_of_deleted_characters()
                nb_fin_cha = single_instance.number_of_final_characters()
                nb_words = single_instance.number_of_words()
                nb_rev = single_instance.number_of_revisions()
                nb_words = single_instance.number_of_words()
                nb_pauses = single_instance.number_of_pauses()
                nb_p_bursts = single_instance.number_p_bursts()
                nb_r_bursts = single_instance.number_r_bursts()

                """
                Outcome variables
                """

                add_char_us_mess_dur = single_instance.add_char_user_mess_dur()
                add_char_us_turn_dur = single_instance.add_char_user_turn_dur()
                fin_char_mess_dur = single_instance.fin_char_us_mess_dur()
                fin_char_turn_dur = single_instance.fin_char_us_turn_dur()
                words_mess_dur = single_instance.words_mess_dur()
                words_turn_dur = single_instance.words_turn_dur()
                mean_p_duration = single_instance.mean_pause_duration()
                p_percent = single_instance.percentage_pause_time()
                rev_ep_mess_dur = single_instance.revision_episodes_mess_dur()
                rev_ep_turn_dur = single_instance.revision_episodes_turn_dur()
                rev_ep_w = single_instance.revision_episodes_per_words()
                rev_prop = single_instance.revision_proportion()
                mean_p_bursts = single_instance.mean_duration_p_bursts()
                mean_r_bursts = single_instance.mean_duration_r_bursts()
                mean_l_p_butsts = single_instance.mean_length_p_bursts()
                mean_l_r_bursts = single_instance.mean_length_r_bursts()
                """
                printing the test messages
                """

                if single_instance.messagenumber == 11712:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)
                if single_instance.messagenumber == 33861:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)
                if single_instance.messagenumber == 74213:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)
                if single_instance.messagenumber == 34423:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)
                if single_instance.messagenumber == 56869:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)


if __name__ == '__main__' :
    matches = turnFactory()
    matches.initialiseFactory('logs_events.json') #initialize the factory using the CSV file
    matches.getTurns()
    matches.identifyTurns()
