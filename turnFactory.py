"""
Author: Daria Alexander
"""

import csv
import pandas as pd
import numpy as nm

# we import the class eventLogLoader
import eventLogLoader as logload

import messageHandling as mymessage
from datetime import datetime
from datetime import timedelta
#import datetime

class turnFactory():

    #constructor
    def __init__(self):
        self.data = None
        self.messagelist =  {} # a dictionnary of messages

    # initialise the methods from EventLogLoader

    def initialiseFactory(self, path_json):
        try:
            initializer = logload.eventLogLoader()
            initializer.load(path_json)
            initializer.extractLengthDiff()
            initializer.extractPauses()
            initializer.revisions()
            initializer.nb_add_char()
            initializer.measures_columns()
            self.data = initializer.getEvents()
        except (FileNotFoundError, IOError):
            print("File not found")

    def getTurns(self):
        return

    def identifyTurns(self):

        # We obtain series, that contain the messages uniques id's

        turnId_series = self.data['msg_id'].unique()
        #print(len(turnId_series))

        #a loop over all the turn id's
        for turnId in turnId_series:
            #we transfer the contents to the messageHandling class
            if not nm.isnan(turnId): # changer: np
                contents = self.data.loc[self.data['msg_id'] == turnId] # cоздаём экземпляр класса и загр туда информацию
                # которую фильтрцем по номеру сообщения
                currentmessage = mymessage.messageHandling(contents, turnId)# эту информацию передаём конструктору messageHandling
                self.messagelist[turnId] = currentmessage

       # b = datetime.datetime(1,1,1)
        load = logload.eventLogLoader()
        b = datetime(1,1,1)
        previousmessage = None
        with open ('results.csv', 'w') as csv_file:
            writer = csv.writer(csv_file, delimiter=';')
            need_write_fields = True
            for k, single_instance in self.messagelist.items(): # we loop over all the keys in the dictionnary messagelist
                single_instance.previousmessage = previousmessage # single_instance belongs to the messageHandling class
                previousmessage = single_instance

                """
                Basic variables
                """
                sub_t = single_instance.user_message_submit_time()
                #print("submit time", turnId, sub_t, 'message', single_instance.messagenumber)
                first_typ = single_instance.first_typing_event()
                #print("first typing event",turnId, first_typ, 'message', single_instance.messagenumber)
                rec_t = single_instance.system_message_reception_time()
                #print("system message reception time wr", turnId, rec_t, 'message', single_instance.messagenumber)
                us_mess_dur = single_instance.user_message_duration()
                #print("user message duration", turnId, us_mess_dur, 'message', single_instance.messagenumber)
                turn_paus_dur = single_instance.user_turn_pause_duration()
                #print("user turn duration", turn_paus_dur, 'message', single_instance.messagenumber)
                us_turn_dur = single_instance.user_turn_duration()
                #print("user turn duration", us_turn_dur, 'message', single_instance.messagenumber)
                sys_turn_dur = single_instance.system_turn_duration()
                #print(sys_turn_dur, 'message', single_instance.messagenumber)
                tot_turn_dur = single_instance.total_turn_duration()
                #print(tot_turn_dur, 'message', single_instance.messagenumber)
                pause_dur = single_instance.pauses_duration()
                #print(pause_dur, 'message', single_instance.messagenumber)
                p_bursts = single_instance.extract_p_bursts()
                revisions = single_instance.revisions_duration()
                r_bursts = single_instance.extract_r_bursts()
                nb_add_char = single_instance.number_of_added_characters()
                nb_del_char = single_instance.number_of_deleted_characters()
                nb_fin_cha = single_instance.number_of_final_characters()
                nb_words = single_instance.number_of_words()
                nb_rev = single_instance.number_of_revisions()
                nb_words = single_instance.number_of_words()
                nb_pauses = single_instance.number_of_pauses()
                nb_p_bursts = single_instance.number_p_bursts()
                nb_r_bursts = single_instance.number_r_bursts()

                """
                Outcome variables
                """

                mean_p_duration = single_instance.mean_pause_duration()
                mean_p_bursts = single_instance.mean_duration_p_bursts()
                mean_r_bursts = single_instance.mean_duration_r_bursts()
                active_wr_dur_prop = single_instance.active_writing_duration_proportion()
                mean_l_p_butsts = single_instance.mean_length_p_bursts()
                mean_l_r_bursts = single_instance.mean_length_r_bursts()
                add_char_act_dur = single_instance.add_char_act_dur()
                words_act_dur = single_instance.words_act_dur()
                m = self.data['msg_id'] == single_instance.messagenumber
                self.data.loc[m, 'words_act_dur'] = self.data.loc[m, 'words_act_dur'].replace(0, words_act_dur)
                add_char_us_turn_dur = single_instance.add_char_user_turn_dur()
                fin_char_act_dur = single_instance.fin_char_act_wr_dur()
                rev_ep_act_dur = single_instance.revision_episodes_active_wr()
                rev_ep_w = single_instance.revision_episodes_per_words()
                rev_prop = single_instance.revision_proportion()

                """
                printing the test messages
                """
                if single_instance.messagenumber == 35589:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)
                if single_instance.messagenumber == 57428:
                    print('message', single_instance.messagenumber, 'result', single_instance.result)

                """
                creating a csv file
                """

                writer = csv.writer(csv_file, delimiter=';')
                writer.writerow(['event_id'] + (list(single_instance.result.keys())))
                list_val = [single_instance.messagenumber] # the list of keys
                for key, value in single_instance.result.items():
                    if type(value) == type(pd._libs.tslibs.timestamps.Timestamp(0)):
                        list_val.append(value)
                    elif type(value) == type(pd._libs.tslibs.timestamps.Timedelta(0)): # transform timedelta into seconds
                        list_val.append(value.value/10**9) # nano seconds
                    else:
                        list_val.append(value) # if not timedelta, append the value the way it is
#                writer.writerow([single_instance.messagenumber, single_instance.result])
                writer.writerow(list_val)
                
    def saveData(self):
        #w = self.data.to_csv("te2.csv", sep=';')
        dat = self.data[self.data.event_type_t == 'Player message']
        da = dat[['msg_id','user_id','msg_text','words_act_dur']]
        datt = da.loc[da['words_act_dur'] > 0]
        c = datt.to_csv("te_column.csv", sep=';')

if __name__ == '__main__' :
    matches = turnFactory()
    matches.initialiseFactory('logs_events.json') #initialize the factory using the CSV file
    matches.getTurns()
    matches.identifyTurns()
    #matches.saveData()
    #print(matches)
